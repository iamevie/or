use crate::token::*;
use crate::span::Span;

pub struct Lexer<'a> {
	source: &'a [u8],
	read: usize,
	current: u8,
	bol: usize,
	line: usize,
}

impl Iterator for Lexer<'_> {
	type Item = (Token, Span);

	fn next(&mut self) -> Option<Self::Item> {
		self.skip_whitespace();

		let mut span = Span { from: (self.line, self.read - self.bol), till: (self.line, self.read - self.bol) };
		let token = match self.current {

			b'(' => Token::LParen,
			b')' => Token::RParen,
			b'[' => Token::LBracket,
			b']' => Token::RBracket,
			b'{' => Token::LSquirly,
			b'}' => Token::RSquirly,

			b'.' => Token::Dot,
			b',' => Token::Comma,
			b':' => Token::Colon,
			b';' => Token::SemiColon,
			b'<' => {
				if self.peek() == b'=' { self.advance(); span.till.1 += 1; Token::LessEqual }
				else { Token::LessThen }
			},
			b'>' => {
				if self.peek() == b'=' { self.advance(); span.till.1 += 1; Token::MoreEqual }
				else { Token::MoreThen }
			},
			b'=' => {
				if self.peek() == b'=' { self.advance(); span.till.1 += 1; Token::Equal }
				else { Token::Assign }
			},
			b'!' => {
				if self.peek() == b'=' { self.advance(); span.till.1 += 1; Token::NotEqual }
				else { Token::Bang }
			},
			b'|' => {
				if self.peek() == b'|' { self.advance(); span.till.1 += 1; Token::Or }
				else { Token::Line }
			},
			b'&' => {
				if self.peek() == b'&' { self.advance(); span.till.1 += 1; Token::And }
				else { Token::Ampersand }
			},
			b'*' => Token::Asterisk,
			b'?' => Token::Questionmark,
			b'+' => Token::Plus,
			b'-' => Token::Dash,
			b'^' => Token::Caret,
			b'%' => Token::Percent,
			
			b'/' => {
				if self.peek() == b'/' {
					self.advance();
					let start = self.read + 1;
					while self.peek() != b'\n' && self.peek() != b'\0' { self.advance() }
					span.till.1 = self.read - self.bol;

					Token::Comment(String::from_utf8(self.source[start..=self.read]
						.to_vec())
						.expect("Failed to parse UTF-8"))
				} else if self.peek() == b'*' {
					self.advance();
					let start = self.read + 1;
					while self.peek() != b'*' || self.peek_by(2) != b'/' && self.peek() != b'\0' {
						if self.peek() == b'\n' { self.line += 1; self.bol = self.read; }
						self.advance();
					}
					span.till = (self.line, self.read - self.bol);
					self.advance_by(2);

					Token::Comment(String::from_utf8(self.source[start..=self.read - 1]
						.to_vec())
						.expect("Failed to parse UTF-8"))
				} else { Token::Slash }
			},

			b'"' => {
				self.advance();
				let start = self.read;
				while self.current != b'"' {
					self.advance();
					if self.current == b'\0' { panic!("Unclosed string") }
				}

				span.till = (self.line, self.read - self.bol);
				Token::String(String::from_utf8(self.source[start..self.read]
					.to_vec())
					.expect("Failed to parse UTF-8"))
			}

			b'a'..=b'z' | b'A'..=b'Z' | b'_' => {
				let start = self.read;
				while self.peek().is_ascii_digit()
					|| self.peek().is_ascii_lowercase()
					|| self.peek().is_ascii_uppercase()
					|| self.peek() == b'_' { self.advance() }

				span.till = (self.line, self.read - self.bol);
				lookup(&self.source[start..=self.read])
			}
			b'0'..=b'9' => {
				fn parse_digit(lexer: &mut Lexer) -> usize {
					let start = lexer.read;
					while lexer.peek().is_ascii_digit() { lexer.advance() }

					match String::from_utf8_lossy(&lexer.source[start..=lexer.read]).parse::<usize>() {
						Ok(number) => number,
						Err(e) => panic!("Failed to parse number, {e}"),
					}
				}

				let lhs = parse_digit(self);
				let res = if self.peek() == b'.' {
					self.advance_by(2);
					Token::Float(lhs, parse_digit(self))
				} else {
					Token::Number(lhs)
				};

				span.till = (self.line, self.read - self.bol);
				res
			}

			b'\0' => return None,			
			_ => Token::Illegal(self.current as char),
		};

		self.advance();
		Some((token, span))
	}
}

impl<'a> Lexer<'a> {
	pub fn new(source: &'a str) -> Self {
		let source = source.as_bytes();
		let read = 0;
		let bol = usize::MAX;
		let line = 1;
		let current = if source.len() == 0 { b'\0' } else { source[0] };

		Self { source, read, current, bol, line }
	}

	#[inline(always)]
	fn skip_whitespace(&mut self) {
		loop {
			match self.current {
				b'\n' => { self.bol = self.read; self.line += 1; },
				b'\t' | b' ' | b'\r' => {},
				_ => break,
			}

			self.advance();
		}
	}

	#[inline(always)]
	fn advance_by(&mut self, by: usize) {
		self.read += by;
		self.current = self[self.read];
	}

	#[inline(always)]
	fn advance(&mut self) {
		self.read += 1;
		self.current = self[self.read];
	}

	#[inline(always)]
	fn peek_by(&self, by: usize) -> u8 {
		self[self.read + by]
	}

	#[inline(always)]
	fn peek(&self) -> u8 {
		self[self.read + 1]
	}
}

impl std::ops::Index<usize> for Lexer<'_> {
	type Output = u8;

	fn index(&self, index: usize) -> &Self::Output {
		if index > self.source.len() - 1 { &b'\0' }
		else { &self.source[index] }
	}
}
