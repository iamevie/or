#[derive(Debug, Clone, Default)]
pub struct Span {
	pub from: (usize, usize),
	pub till: (usize, usize),
}
