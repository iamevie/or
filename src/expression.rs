use crate::parser::*;
use crate::token::Token;
use crate::scope::Scope;
use crate::precedence::Precedence;
use crate::type_symbol::TypeSymbol;

#[derive(Debug)]
pub enum Expression {
	Number(usize),
	Float(usize, usize),
	Ident(String),
	String(String),
	Tuple(Vec<Box<Expression>>),

	FunctionCall { ident: String, fields: Vec<Box<Expression>> },
	Member { ident: String, member: Box<Expression> },
	PrePrefix(Token, Box<Expression>),
	BinaryOperation { lhs: Box<Expression>, rhs: Box<Expression>, op: Token },
	If { ifs: Vec<(Box<Expression>, Scope)>, _else: Scope },
	For { lhs: Box<Expression>, rhs: Box<Expression>, scope: Scope },
	While { condition: Box<Expression>, scope: Scope },
	Loop { scope: Scope },
	StructInit { ident: String, fields: Vec<(String, Box<Expression>)> },
	Cast { expression: Box<Expression>, symbol: TypeSymbol }
}

impl Parser {
	pub fn expression(&mut self, precedence: Precedence) -> ParserResult<Box<Expression>> {
		if self.current == Token::SemiColon { return Err((0, ParserError::ExpectedExpression)) }
		let mut res = self.prefix()?;

		while precedence <= self.precedence(self.peek().clone())? {
			if self.peek() == &Token::Eos { return Err((0, ParserError::UnclosedExpression)) }
			else if self.peek() == &Token::SemiColon { break; }
			self.advance();
			res = self.infix(res)?;
		}

		Ok(res)
	}

	fn prefix(&mut self) -> ParserResult<Box<Expression>> {
		Ok(Box::new(match &self.current {
			Token::Number(num) => Expression::Number(*num),
			Token::Float(lhs, rhs) => Expression::Float(*lhs, *rhs),
			Token::Ident(_) => self.ident()?,
			Token::String(string) => Expression::String(string.to_string()),
			Token::Dash | Token::Plus | Token::Bang => {
				let pre = self.current.clone();
				self.advance();
				Expression::PrePrefix(pre, self.prefix()?)
			},
			Token::If => self._if()?,
			Token::Match => self._match()?,
			Token::While => self._while()?,
			Token::For => self._for()?,
			Token::Loop => self._loop()?,
			Token::LParen => {
				self.advance();
				let expression = self.expression(Precedence::None)?;
				self.advance();
				if self.current == Token::Comma { self.tuple(expression)? }
				else if self.current != Token::RParen {
					return Err((0, ParserError::ExpectedButFound {
						expected: Token::RParen,
						found: self.current.clone()
					}));
				}
				else { *expression }
			},

			_ => return Err((0, ParserError::InvalidPrefixExpression(self.current.clone())))
		}))
	}

	fn infix(&mut self, lhs: Box<Expression>) -> ParserResult<Box<Expression>> {
		let current = self.current.clone();
		self.advance();
		let precedence = self.precedence(current.clone())?;
		let rhs = self.expression(precedence)?;

		Ok(Box::new(match current {
			Token::Plus | Token::Dash | Token::Asterisk | Token::LessThen
			| Token::MoreThen | Token::Equal | Token::NotEqual | Token::Slash
			| Token::Percent | Token::MoreEqual | Token::LessEqual
			=> Expression::BinaryOperation { lhs, rhs, op: current },
			_ => return Err((0, ParserError::InvalidExpressionKind(current.clone())))
		}))
	}

	fn _if(&mut self) -> ParserResult<Expression> {
		self.advance();
		let expression = self.expression(Precedence::None)?;
		self.advance();
		let mut ifs = vec!((expression, self.scope()?));
		let mut _else = Scope::default();
		while self.current == Token::Else {
			if self.current == Token::Eos { return Err((0, ParserError::ExpectedButFound {
				expected: Token::Else,
				found: Token::Eos,
			}))}

			self.advance();
			if self.current == Token::If {
				self.advance();
				let expression = self.expression(Precedence::None)?;
				self.advance();
				ifs.push((expression, self.scope()?));
			} else {
				_else = self.scope()?;
				break;
			}
		}

		Ok(Expression::If { ifs, _else })
	}

	fn ident(&mut self) -> ParserResult<Expression> {
		let ident = if let Token::Ident(ident) = &self.current { ident.clone() }
		else { unreachable!() };

		Ok(match self.peek() {
			Token::LParen => {
				self.advance_by(2);
				let mut fields = Vec::new();
				while self.current != Token::RParen {
					if self.current == Token::Eos { return Err((0, ParserError::ExpectedButFound {
						expected: Token::RParen,
						found: Token::Eos,
					}))} else if self.current == Token::SemiColon { return Err((0, ParserError::ExpectedButFound {
						expected: Token::RParen,
						found: Token::SemiColon,
					}))}

					fields.push(self.expression(Precedence::None)?);
					self.advance();
					if self.current != Token::RParen
					&& self.current != Token::SemiColon
					&& self.current != Token::Eos { self.expect_and_advance(Token::Comma)?; }
				}
				Expression::FunctionCall { ident, fields }
			},
			Token::Dot => {
				self.advance_by(2);
				let member = self.expression(Precedence::None)?;

				Expression::Member { ident, member }
			},
			Token::LSquirly => {
				self.advance_by(2);
				let mut fields = Vec::new();
				while self.current != Token::RSquirly {
					if self.current == Token::Eos { return Err((0, ParserError::ExpectedButFound {
						expected: Token::RSquirly,
						found: Token::Eos,
					}))}

					let name = self.expect_ident()?;
					let expression = if self.current == Token::Colon {
						self.advance();
						let expression = self.expression(Precedence::None)?;
						self.advance();
						expression
					} else { Box::new(Expression::Ident(name.clone())) };
					fields.push((name, expression));

					if self.current != Token::RSquirly
					&& self.current != Token::SemiColon
					&& self.current != Token::Eos { self.expect_and_advance(Token::Comma)?; }
				}

				Expression::StructInit { ident, fields }
			},
			_ => Expression::Ident(ident)
		})
	}

	fn _match(&mut self) -> ParserResult<Expression> { todo!() }

	fn _while(&mut self) -> ParserResult<Expression> {
		self.advance();
		let condition = self.expression(Precedence::None)?;
		self.advance();
		let scope = self.scope()?;

		Ok(Expression::While { condition, scope })
	}

	fn _for(&mut self) -> ParserResult<Expression> {
		self.advance();
		let lhs = self.expression(Precedence::None)?;
		self.advance();
		self.expect_and_advance(Token::In)?;
		let rhs = self.expression(Precedence::None)?;
		self.advance();
		let scope = self.scope()?;

		Ok(Expression::For { lhs, rhs, scope })
	}

	fn _loop(&mut self) -> ParserResult<Expression> {
		self.advance();
		let scope = self.scope()?;

		Ok(Expression::Loop { scope })
	}

	fn tuple(&mut self, first: Box<Expression>) -> ParserResult<Expression> {
		self.advance();
		let mut body = vec![first];
		while self.current != Token::RParen {
			if self.current == Token::Eos { return Err((0, ParserError::ExpectedButFound {
				expected: Token::RParen,
				found: Token::Eos,
			}))};

			body.push(self.expression(Precedence::None)?);
			self.advance();
			if self.current != Token::RParen { self.expect_and_advance(Token::Comma)?; }
		}

		Ok(Expression::Tuple(body))
	}
}
