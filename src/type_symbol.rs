use crate::parser::*;
use crate::token::Token;

#[derive(Debug, Default)]
pub struct TypeSymbol {
	pub reference: bool,
	pub mutable: bool,
	pub kind: TypeSymbolKind,
}

#[derive(Debug, Default)]
pub enum TypeSymbolKind {
	Tuple(Vec<TypeSymbol>),
	Ident(String),
	GenericIdent(String, Box<TypeSymbol>),
	Array(Box<TypeSymbol>, usize),
	String,
	Unsigned8,
	Unsigned16,
	Unsigned32,
	Unsigned64,
	Unsigned128,
	UnsignedSize,
	Signed8,
	Signed16,
	Signed32,
	Signed64,
	Signed128,
	SignedSize,
	Float32,
	Float64,
	Float128,
	FloatSize,
	Bool,
	#[default]
	Infer,
}

impl Parser {
	pub fn symbol(&mut self) -> ParserResult<TypeSymbol> {
		let reference = if self.current == Token::Ampersand { self.advance(); true } else { false };
		let mutable = if self.current == Token::Mutable { self.advance(); true } else { false };

		let kind = if self.current == Token::LBracket {
			self.advance();
			let symbol = self.symbol()?;
			self.expect_and_advance(Token::SemiColon)?;
			let size = self.expect_number()?;

			TypeSymbolKind::Array(Box::new(symbol), size)
		} else if self.current == Token::LParen {
			self.advance();
			let mut symbols = Vec::new();
			while self.current != Token::RParen {
				if self.current == Token::Eos { return Err((0, ParserError::ExpectedButFound {
					expected: Token::RParen,
					found: Token::Eos
				}))}

				symbols.push(self.symbol()?);
				if self.current != Token::RParen { self.expect_and_advance(Token::Comma)?; }
			}

			TypeSymbolKind::Tuple(symbols)
		} else if let Token::Ident(ident) = &self.current {
			match ident.as_ref() {
				"u8" => TypeSymbolKind::Unsigned8,
				"u16" => TypeSymbolKind::Unsigned16,
				"u32" => TypeSymbolKind::Unsigned32,
				"u64" => TypeSymbolKind::Unsigned64,
				"u128" => TypeSymbolKind::Unsigned128,
				"usize" => TypeSymbolKind::UnsignedSize,
				"s8" => TypeSymbolKind::Signed8,
				"s16" => TypeSymbolKind::Signed16,
				"s32" => TypeSymbolKind::Signed32,
				"s64" => TypeSymbolKind::Signed64,
				"s128" => TypeSymbolKind::Signed128,
				"size" => TypeSymbolKind::SignedSize,
				"f32" => TypeSymbolKind::Float32,
				"f64" => TypeSymbolKind::Float64,
				"f128" => TypeSymbolKind::Float128,
				"fsize" => TypeSymbolKind::FloatSize,
				"bool" => TypeSymbolKind::Bool,
				"string" => TypeSymbolKind::String,

				_ => TypeSymbolKind::Ident(ident.clone())
			}
		} else { return Err((0, ParserError::ExpectedTypeSymbol)); };

		self.advance();
		Ok(TypeSymbol { reference, mutable, kind })
	}
}
