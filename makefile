COMPILER := rustc
C_ARGS := --edition=2021
NAME := orc

SRC_DIR := src
LIB_DIR := lib

BIN_DIR := bin
DEBUG_DIR := $(BIN_DIR)/debug
RELEASE_DIR := $(BIN_DIR)/release
EXAMPLE := example/main.or

default: run

clean:
	@rm -rf $(BIN_DIR)

build:
	@mkdir -p $(RELEASE_DIR)
	@$(COMPILER) $(C_ARGS) $(SRC_DIR)/main.rs -o $(RELEASE_DIR)/$(NAME) -O

build-debug:
	@mkdir -p $(DEBUG_DIR)
	@$(COMPILER) $(C_ARGS) $(SRC_DIR)/main.rs -o $(DEBUG_DIR)/$(NAME)

run:
	@if [ $(SRC_DIR) -nt $(RELEASE_DIR)/$(NAME) ]; then make build; fi;
	@$(RELEASE_DIR)/$(NAME) $(EXAMPLE)

run-debug:
	@if [ $(SRC_DIR) -nt $(DEBUG_DIR)/$(NAME) ]; then make build-debug; fi;
	@$(DEBUG_DIR)/$(NAME) $(EXAMPLE)

.PHONY: clean build build_debug run run_debug
