use or::token::Token;
use or::parser::Parser;

fn run(src: &str, tokens: &[Token]) {

	let lexer = Parser::new(src);

	lexer.enumerate().for_each(|(i, t)| {
		assert_eq!(t.0, tokens[i])
	})
}
