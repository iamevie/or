use crate::node::Node;
use crate::parser::Parser;

pub struct Ast {
	nodes: Vec<Node>,
}

impl Ast {
	pub fn new(file: String) -> Self {
		let nodes: Vec<Node> = Parser::new(file.clone()).collect();
		nodes.iter().for_each(|n| { dbg!(&file, n); println!() });

		Self { nodes }
	}
}
