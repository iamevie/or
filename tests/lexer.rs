use or::token::Token;
use or::lexer::Lexer;

fn run(src: &str, tokens: &[Token]) {
	let lexer = Lexer::new(src);

	lexer.enumerate().for_each(|(i, t)| {
		assert_eq!(t.0, tokens[i])
	})
}

#[test]
fn single_token() {
	run("-=/{+*&](|)}.,:;<>!^%", &[
		Token::Dash,
		Token::Assign,
		Token::Slash,
		Token::LSquirly,
		Token::Plus,
		Token::Asterisk,
		Token::Ampersand,
		Token::RBracket,
		Token::LParen,
		Token::Line,
		Token::RParen,
		Token::RSquirly,
		Token::Dot,
		Token::Comma,
		Token::Colon,
		Token::SemiColon,
		Token::LessThen,
		Token::MoreThen,
		Token::Bang,
		Token::Caret,
		Token::Percent,
	])
}

#[test]
fn double_token() {
	run("==! !=, &&, |, ||, <= >=", &[
		Token::Equal,
		Token::Bang,
		Token::NotEqual,
		Token::Comma,
		Token::And,
		Token::Comma,
		Token::Line,
		Token::Comma,
		Token::Or,
		Token::Comma,
		Token::LessEqual,
		Token::MoreEqual
	])
}

#[test]
fn idents() {
	run("i love \"ducks\" as much as octopus", &[
		Token::Ident("i".to_owned()),
		Token::Ident("love".to_owned()),
		Token::String("ducks".to_owned()),
		Token::As,
		Token::Ident("much".to_owned()),
		Token::As,
		Token::Ident("octopus".to_owned()),
	])
}

#[test]
fn keywords() {
	run("import as fn return break loop while for if else impl pub struct enum mut else match in", &[
		Token::Import,
		Token::As,
		Token::Function,
		Token::Return,
		Token::Break,
		Token::Loop,
		Token::While,
		Token::For,
		Token::If,
		Token::Else,
		Token::Implement,
		Token::Public,
		Token::Struct,
		Token::Enum,
		Token::Mutable,
		Token::Else,
		Token::Match,
		Token::In,
	])
}
