use crate::token::Token;
use crate::lexer::Lexer;
use crate::span::Span;
pub type ParserResult<T> = Result<T, (usize, ParserError)>;

#[derive(Debug)]
pub enum ParserError {
	UnexpectedPublicKeyword,
	ExpectedExpressionFound(Token),
	InvalidExpressionKind(Token),
	InvalidPrefixExpression(Token),
	Unimplemented(Token),
	ExpectedStatementFound(Token),
	IllegalToken(char),
	ExpectedName,
	UnclosedExpression,
	ExpectedTypeSymbol,
	ExpectedNumber,
	ExpectedExpression,
	ExpectedButFound { expected: Token, found: Token },
	ExpectedOneOfButFound { expected: Vec<Token>, found: Token },
	ExpectedOneOfMultipleButFound { expected: Vec<Vec<Token>>, found: Vec<Token> },
	TokenHasNoPrecedence(Token),
}

#[derive(Debug)]
pub struct Parser {
	pub file: String,
	pub source: Vec<(Token, Span)>,
	pub read: usize,
	pub current: Token,
}

impl<'a> Parser {
	pub fn new(file: String) -> Self {
		let mut source: Vec<(Token, Span)> = Lexer::new(&std::fs::read_to_string(file.clone()).unwrap()).filter_map(|t| {
			// println!("{t:?}");
			if let Token::Comment(_) = t.0 { None }
			else { Some(t) }
		}).collect();
		source.push((Token::Eos, source[source.len() - 1].1.clone()));
		let current = if source.len() == 0 { Token::Eos } else { source[0].0.clone() };

		Self { file, source, read: 0, current }
	}

	pub fn expect_and_advance(&mut self, token: Token) -> ParserResult<()> {
		if self.current != token { Err((0, ParserError::ExpectedButFound { expected: token, found: self.current.clone()})) }
		else { self.advance(); Ok(()) }
	}

	pub fn expect_one_of_and_advance(&mut self, tokens: Vec<Token>) -> ParserResult<usize> {
		for (i, t) in tokens.iter().enumerate() {
			if self.peek_by(i) != t { continue; }
			
			self.advance();
			return Ok(i)
		}
		
		Err((0, ParserError::ExpectedOneOfButFound { expected: tokens, found: self.current.clone()}))
	}

	pub fn expect_one_of_multiple_and_advance(&mut self, tokens: Vec<&[Token]>) -> ParserResult<usize> {
		'outer: for (r, ts) in tokens.iter().enumerate() {
			for (i, t) in ts.iter().enumerate() {
				if self.peek_by(i) != t { continue 'outer; }
			}
			
			self.advance_by(ts.len());
			return Ok(r)
		}
		
		let mut found = Vec::new();
		let mut len = 0;
		tokens.iter().for_each(|e| if e.len() > len { len = e.len() });
		for i in 1..len + 1 { found.push(self.peek_by(i).clone()) }
		Err((0, ParserError::ExpectedOneOfMultipleButFound {
				expected: tokens.iter().map(|e| e.to_vec()).collect::<Vec<Vec<Token>>>(),
				found
		}))
	}

	pub fn expect_ident(&mut self) -> ParserResult<String> {
		let res = match &self.current {
			Token::Ident(name) => Ok(name.clone()),
			_ => Err((0, ParserError::ExpectedName))
		};

		if let Ok(_) = res { self.advance() }
		res
	}

	pub fn expect_string(&mut self) -> ParserResult<String> {
		let res = match &self.current {
			Token::String(name) => Ok(name.clone()),
			_ => Err((0, ParserError::ExpectedButFound {
				expected: Token::String("string".to_string()),
				found: self.current.clone()
			}))
		};

		if let Ok(_) = res { self.advance() }
		res
	}

	pub fn expect_number(&mut self) -> ParserResult<usize> {
		let res = match &self.current {
			Token::Number(number) => Ok(number.clone()),
			_ => Err((0, ParserError::ExpectedNumber))
		};

		if let Ok(_) = res { self.advance() }
		res
	}

	#[inline(always)]
	pub fn advance(&mut self) {
		self.read += 1;
		self.current = self[self.read].0.clone();
	}

	#[inline(always)]
	pub fn advance_by(&mut self, by: usize) {
		self.read += by;
		self.current = self[self.read].0.clone();
	}

	#[inline(always)]
	pub fn peek(&'a self) -> &'a Token {
		&self[self.read + 1].0
	}

	#[inline(always)]
	pub fn peek_by(&'a self, by: usize) -> &'a Token {
		&self[self.read + by].0
	}
}

impl std::ops::Index<usize> for Parser {
	type Output = (Token, Span);

	fn index(&self, index: usize) -> &Self::Output {
		if index >= self.source.len() - 1 {  &self.source[self.source.len() - 1]}
		else { &self.source[index] }
	}
}
