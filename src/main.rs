use or::ast::Ast;
use or::PARSERS;
use or::FORMATER;

fn main() {
	let mut args = std::env::args();
	args.next();
	
	let file = args.next().unwrap();
	let main_ast = Ast::new(file.clone());

	unsafe { PARSERS.push((file, main_ast)); }
	unsafe { FORMATER.err_count(); }
}
