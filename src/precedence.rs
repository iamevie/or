use crate::parser::*;
use crate::token::Token;

#[derive(PartialEq, PartialOrd)]
pub enum Precedence {
	Duck,
	None,
	Comparison,
	Sum,
	Product,
	Exponent,
	Controlflow,
	Cast,
}

impl Parser {
	pub fn precedence(&mut self, token: Token) -> ParserResult<Precedence> {
		Ok(match token {
			Token::Equal | Token::NotEqual | Token::LessThen | Token::MoreThen
			| Token::MoreEqual | Token::LessEqual => Precedence::Comparison,
			Token::Plus | Token::Dash => Precedence::Sum,
			Token::Asterisk | Token::Slash => Precedence::Product,
			Token::Caret => Precedence::Exponent,
			Token::If | Token::Match | Token::For | Token::Loop => Precedence::Controlflow,

			_ => Precedence::Duck,
			// _ => return Err(ParserError::TokenHasNoPrecedence(self.current.clone()))
		})
	}
}
