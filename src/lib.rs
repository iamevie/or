pub mod token;
pub mod lexer;
pub mod parser;
mod statement;
mod expression;
mod node;
mod scope;
mod type_symbol;
mod span;
mod precedence;
mod format;
pub mod ast;

use crate::format::Formater;
use crate::ast::Ast;

pub static mut FORMATER: Formater = Formater { count: 0 };
pub static mut PARSERS: Vec<(String, Ast)> = Vec::new();

pub fn panic() {
	unsafe { FORMATER.err_count(); }
	panic!("");
}
