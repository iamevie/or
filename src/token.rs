pub fn lookup(ident: &[u8]) -> Token {
	match ident {
		b"fn" => Token::Function,
		b"pub" => Token::Public,
		b"struct" => Token::Struct,
		b"enum" => Token::Enum,
		b"mut" => Token::Mutable,
		b"impl" => Token::Implement,
		b"let" => Token::Let,
		b"if" => Token::If,
		b"else" => Token::Else,
		b"match" => Token::Match,
		b"while" => Token::While,
		b"for" => Token::For,
		b"loop" => Token::Loop,
		b"return" => Token::Return,
		b"break" => Token::Break,
		b"in" => Token::In,
		b"as" => Token::As,
		b"import" => Token::Import,

		_ => Token::Ident(String::from_utf8(ident.to_vec()).expect("Found invalid UTF-8"))
	}
}

#[derive(Debug, Clone, PartialEq)]
pub enum Token {
	Eos,
	Illegal(char),	// Well, Illegal
	
	Ident(String),
	String(String),
	Number(usize),
	Float(usize, usize),
	Comment(String),

	LParen,			// (
	RParen,			// )
	LSquirly,		// {
	RSquirly,		// }
	LBracket,		// [
	RBracket,		// ]
	
	Dot,			// .
	Comma,			// ,
	Colon,			// :
	SemiColon,		// ;
	LessThen,		// <
	MoreThen,		// >
	Assign,			// =
	Bang,			// !
	Ampersand, 		// &
	Line,			// |
	Asterisk,		// *
	Questionmark,	// ?
	Plus,			// +
	Dash, 			// -
	Slash,			// /
	Caret,			// ^
	Percent,		// %

	Equal,			// ==
	NotEqual,		// != 
	LessEqual,		// <=
	MoreEqual,		// >=
	And,			// &&
	Or,				// ||
	
	Function,		// fn
	Public,			// pub
	Struct,			// struct
	Enum,			// enum
	Mutable,		// mut
	Implement,		// impl
	Let,			// let
	If,				// if
	Else,			// else
	Match,			// match
	While,			// while
	For,			// for
	Loop,			// loop
	Return,			// return
	Break,			// break
	In,				// in
	As,				// as
	Import,			// import
}

use std::fmt::*;
impl Display for Token {
	fn fmt(&self, f: &mut Formatter<'_>) -> Result<> {
		write!(f, "{}", match self {
			Token::Eos => "eos",
			Token::Illegal(_) => "illegal",
			
			Token::Ident(_) => "ident",
			Token::String(_) => "string",
			Token::Number(_) => "number",
			Token::Float(_, _) => "float",
			Token::Comment(_) => "comment",

			Token::LParen => "(",
			Token::RParen => ")",
			Token::LSquirly => "{",
			Token::RSquirly => "}",
			Token::LBracket => "[",
			Token::RBracket => "]",
			
			Token::Dot => ".",
			Token::Comma => ",",
			Token::Colon => ":",
			Token::SemiColon => ";",
			Token::LessThen => "<",
			Token::MoreThen => ">",
			Token::Assign => "=",
			Token::Bang => "!",
			Token::Ampersand => "&",
			Token::Line => "|",
			Token::Asterisk => "*",
			Token::Questionmark => "?",
			Token::Plus => "+",
			Token::Dash => "-",
			Token::Slash => "/",
			Token::Caret => "^",
			Token::Percent => "%",

			Token::Equal => "==",
			Token::NotEqual => "!= ",
			Token::LessEqual => "<=",
			Token::MoreEqual => ">=",
			Token::And => "&&",
			Token::Or => "||",
			
			Token::Function => "fn",
			Token::Public => "pub",
			Token::Struct => "struct",
			Token::Enum => "enum",
			Token::Mutable => "mut",
			Token::Implement => "impl",
			Token::Let => "let",
			Token::If => "if",
			Token::Else => "else",
			Token::Match => "match",
			Token::While => "while",
			Token::For => "for",
			Token::Loop => "loop",
			Token::Return => "return",
			Token::Break => "break",
			Token::In => "in",
			Token::As => "as",
			Token::Import => "import",
		})
	}
}
