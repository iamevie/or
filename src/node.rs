use crate::expression::Expression;
use crate::statement::Statement;
use crate::parser::*;
use crate::FORMATER;
use crate::panic;
use crate::token::Token;
use crate::precedence::Precedence;

#[derive(Debug)]
pub enum Node {
	Expression(Box<Expression>),
	Statement(Statement),
}

impl Iterator for Parser {
	type Item = Node;

	fn next(&mut self) -> Option<Self::Item> {
		let node = match self.current {
			Token::Function | Token::Struct | Token::Enum | Token::Implement | Token::Let
			| Token::Public | Token::Return | Token::Break | Token::Import
			=> match self.statement() {
				Ok(statement) => Ok(Node::Statement(statement)),
				Err((_, err)) => Err((self.read, err)),
			},

			Token::Illegal(c) => Err((self.read, ParserError::IllegalToken(c))),

			Token::Eos => return None,

			_ => match self.expression(Precedence::None) {
				Ok(expression) => Ok(Node::Expression(expression)),
				Err((_, err)) => Err((self.read - 1, err)),
			},
		};

		if let Ok(Node::Expression(_)) = node { self.advance() }
		else if let Err(err) = node { unsafe { FORMATER.print(self, err); } panic(); return None; }
		Some(node.unwrap())
	}
}
