use crate::node::Node;
use crate::parser::*;
use crate::token::Token;

#[derive(Debug, Default)]
pub struct Scope {
    body: Vec<Node>,
}

impl Parser {
    pub fn scope(&mut self) -> ParserResult<Scope> {
        self.expect_and_advance(Token::LSquirly)?;

        let mut body = Vec::new();
        while self.current != Token::RSquirly {
            if self.current == Token::Eos {
                return Err((
                    0,
                    ParserError::ExpectedButFound {
                        expected: Token::RSquirly,
                        found: Token::Eos,
                    },
                ));
            }

            let node = self.next();
            if let None = node {
                break;
            }

            body.push(node.unwrap());
        }

        Ok(Scope { body })
    }
}
