use crate::parser::*;
use crate::token::Token;
use crate::scope::Scope;
use crate::type_symbol::*;
use crate::expression::Expression;
use crate::precedence::Precedence;
use crate::PARSERS;
use crate::ast::Ast;

#[derive(Debug)]
pub enum Statement {
	Function { public: bool, name: String, parameters: Vec<(String, TypeSymbol)>, symbol: TypeSymbol, scope: Scope },
	Struct { public: bool, name: String, fields: Vec<(String, TypeSymbol)> },
	Enum { public: bool, name: String },
	Implement { public: bool, name: String, scope: Scope },
	Assign { mutable: bool, name: String, symbol: TypeSymbol, expression: Box<Expression> },
	Return { expression: Box<Expression> },
	Break,
	Import { module: String, name: String },
}

impl Parser {
	pub fn statement(&mut self) -> ParserResult<Statement> {
		Ok(match self.current {
			Token::Public => self.public()?,
			Token::Function => self.function()?,
			Token::Struct => self._struct()?,
			Token::Enum => self._enum()?,
			Token::Implement => self.implement()?,
			Token::Return => self._return()?,
			Token::Break => self._break()?,
			Token::Let => self.assign()?,
			Token::Import => self.import()?,

			_ => return Err((0, ParserError::ExpectedStatementFound(self.current.clone())))
		})
	}

	fn public(&mut self) -> ParserResult<Statement> {
		self.advance();

		Ok(match self.statement()? {
			Statement::Function { name, parameters, symbol, scope, .. } => Statement::Function { public: true, name, parameters, symbol, scope },
			Statement::Struct { name, fields, .. } => Statement::Struct { public: true, fields, name },
			Statement::Enum { name, .. } => Statement::Enum { public: true, name },
			Statement::Implement { name, scope, .. } => Statement::Implement { public: true, name, scope },

			_ => return Err((0, ParserError::UnexpectedPublicKeyword)),
		})
	}

	fn function(&mut self) -> ParserResult<Statement> {
		let (name, parameters, symbol) = self.function_signature()?;
		let scope = self.scope()?;
		self.advance();

		Ok(Statement::Function { public: false, name, scope, parameters, symbol })
	}

	fn function_signature(&mut self) -> ParserResult<(String, Vec<(String, TypeSymbol)>, TypeSymbol)> {
		self.advance();
		let name = self.expect_ident()?;

		let parameters = self.parameters(Token::LParen, Token::RParen)?;
		let symbol = if self.current == Token::Colon {
			self.advance();
			self.symbol()?
		} else { TypeSymbol { reference: false, mutable: false, kind: TypeSymbolKind::Tuple(Vec::new()) } };

		Ok((name, parameters, symbol))
	}

	fn parameters(&mut self, start: Token, end: Token) -> ParserResult<Vec<(String, TypeSymbol)>> {
		self.expect_and_advance(start)?;
		let mut parameters = Vec::new();
		while self.current != end {
			if self.current == Token::Eos { return Err((0, ParserError::ExpectedButFound {
				expected: end,
				found: Token::Eos,
			}))}

			let name = self.expect_ident()?;
			self.expect_and_advance(Token::Colon)?;
			parameters.push((name, self.symbol()?));
			if self.current != end { self.expect_and_advance(Token::Comma)?; }
		}

		self.advance();
		Ok(parameters)
	}

	fn assign(&mut self) -> ParserResult<Statement> {
		self.advance();
		let mutable = if self.current == Token::Mutable { self.advance(); true } else { false };
		let name = self.expect_ident()?;
		let symbol = if self.current == Token::Colon {
			self.advance();
			self.symbol()?
		} else { TypeSymbol::default() };

		self.expect_and_advance(Token::Assign)?;
		let expression = self.expression(Precedence::None)?;
		self.advance();
		self.expect_and_advance(Token::SemiColon)?;

		Ok(Statement::Assign { mutable, name, symbol, expression })
	}

	fn _struct(&mut self) -> ParserResult<Statement> {
		self.advance();
		let name = self.expect_ident()?;
		let fields = self.parameters(Token::LSquirly, Token::RSquirly)?;

		Ok(Statement::Struct{ public: false, name, fields })
	}

	fn _enum(&mut self) -> ParserResult<Statement> { todo!() }

	fn implement(&mut self) -> ParserResult<Statement> {
		self.advance();
		let name = self.expect_ident()?;
		let scope = self.scope()?;

		Ok(Statement::Implement { public: false, name, scope })
	}

	fn _return(&mut self) -> ParserResult<Statement> { 
		self.advance();
		let expression = self.expression(Precedence::None)?;
		self.advance();
		self.expect_and_advance(Token::SemiColon)?;

		Ok(Statement::Return { expression })
	}

	fn _break(&mut self) -> ParserResult<Statement> { todo!() }

	fn import(&mut self) -> ParserResult<Statement> {
		self.advance();
		let module = self.expect_string()?;
		let name = if self.current == Token::As { self.advance(); self.expect_ident()? }
		else { module.split('/').last().unwrap().to_string() };
		self.expect_and_advance(Token::SemiColon)?;

		let new_file = format!("{}/{}.or", self.file.clone().split('/').nth(0).unwrap(), module.clone());
		unsafe { if PARSERS.iter().filter(|e| e.0 == new_file).count() == 0 {
			 PARSERS.push((new_file.clone(), Ast::new(new_file.clone())))
		}}

		Ok(Statement::Import { module, name })
	}
}
