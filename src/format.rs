#![allow(unused)]

use crate::parser::*;
use std::fs::read_to_string;
use crate::token::Token;
use crate::span::Span;

const RESET: &'static str = "\x1b[0m";
const BOLD: &'static str = "\x1b[1m";
const FAINT: &'static str = "\x1b[2m";
const UNDERLINE: &'static str = "\x1b[4m";
 
const BLACK_FG: &'static str = "30";
const BLACK_BG: &'static str = "40";
const RED_FG: &'static str = "31";
const RED_BG: &'static str = "41";
const GREEN_FG: &'static str = "32";
const GREEN_BG: &'static str = "42";
const YELLOW_FG: &'static str = "33";
const YELLOW_BG: &'static str = "43";
const BLUE_FG: &'static str = "34";
const BLUE_BG: &'static str = "44";
const MAGENTA_FG: &'static str = "35";
const MAGENTA_BG: &'static str = "45";
const CYAN_FG: &'static str = "36";
const CYAN_BG: &'static str = "46";
const WHITE_FG: &'static str = "37";
const WHITE_BG: &'static str = "47";
 
const BRIGHT_BLACK_FG: &'static str = "90";
const BRIGHT_BLACK_BG: &'static str = "100";
const BRIGHT_RED_FG: &'static str = "91";
const BRIGHT_RED_BG: &'static str = "101";
const BRIGHT_GREEN_FG: &'static str = "92";
const BRIGHT_GREEN_BG: &'static str = "102";
const BRIGHT_YELLOW_FG: &'static str = "93";
const BRIGHT_YELLOW_BG: &'static str = "103";
const BRIGHT_BLUE_FG: &'static str = "94";
const BRIGHT_BLUE_BG: &'static str = "104";
const BRIGHT_MAGENTA_FG: &'static str = "95";
const BRIGHT_MAGENTA_BG: &'static str = "105";
const BRIGHT_CYAN_FG: &'static str = "96";
const BRIGHT_CYAN_BG: &'static str = "106";
const BRIGHT_WHITE_FG: &'static str = "97";
const BRIGHT_WHITE_BG: &'static str = "107";

macro_rules! color {
	($color:ident) => { &format!("\x1b[{}m", $color) };
	($fg:ident, $bg:ident) => { &format!("\x1b[{};{}m", $fg, $bg) };
}

#[derive(Debug, Clone, Default)]
pub struct Formater {
	pub count: usize,
}

impl Formater {
	pub fn err_count(&self) {
		if self.count == 0 { return; }
		eprintln!("{}{}error{}: {} error{} emitted{}", color!(RED_FG), BOLD, color!(WHITE_FG), self.count, if self.count > 1 { "s" } else { "" }, RESET);
	}

	pub fn print(&mut self, parser: &mut Parser, error: (usize, ParserError)) {
		let span = parser[error.0].1.clone();
		let line_idx = span.from.0 - 1;
		self.count += 1;

		let file = read_to_string(parser.file.clone()).unwrap();
		let mut line = file.lines().nth(line_idx).unwrap().to_string();
		let tab_width = line.chars().filter(|c| c == &'\t').count() * 4;
		line = line.replace("\t", "    ");

		let mut from = span.from.1 - 1 + tab_width;
		let mut till = span.till.1 - 1 + tab_width;

		let msg = match error.1 {
			ParserError::ExpectedTypeSymbol => format!("Either missing type symbol such as 'u32' or remove ':' for it to be infered"),
			ParserError::ExpectedExpression => format!("Expected an expresson such as 'true' or '1 + 2 / 50'"),
			ParserError::ExpectedName => format!("Expected an name"),
			ParserError::ExpectedButFound { expected, found } => {
				if found == Token::Eos { from += 1; till += 1; }
				format!("expected: '{expected}' but found: '{found}'")
			},
			_ => format!("{:?}", error.1),
		};
	
		fn code(mut line: String, msg: String, file: String, idx: usize, from: usize, till: usize, span: Span) {
			eprintln!("{}{}error{}: {}", color!(RED_FG), BOLD, color!(WHITE_FG), msg);
			eprintln!("{}{}   --> {}{}:{}:{}", color!(MAGENTA_FG), BOLD, RESET, file, span.from.0, span.from.1);
			eprintln!("{}{}    | ", color!(MAGENTA_FG), BOLD);
			eprint!("{}{}{:>3} | {}", color!(MAGENTA_FG), BOLD, idx, RESET);
			line.push(' ');
			for (i, c) in line.chars().enumerate() {
				if i >= from && i <= till { eprint!("{}{}{c}{}", color!(RED_FG), UNDERLINE, RESET) }
				else { eprint!("{c}"); }
			}
			eprintln!("\n{}{}    | ", color!(MAGENTA_FG), BOLD);
		}

		code(line, msg, parser.file.clone(), line_idx - 1, from, till, span);

		eprintln!("{}\n", RESET);
	}
}
